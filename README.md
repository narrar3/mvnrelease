### What is this repository for? ###

* Maven repository for the Narrar3 components. External users can get the Narrar components from here. This repository contains the "released" versions.

### How do I get set up? ###

In the POM file use:

```
<repositories>
    <repository>
        <id>narrarRel</id>
        <name>narrarRelName</name>
        <releases>
            <enabled>true</enabled>
        </releases>
        <snapshots>
            <enabled>false</enabled>
        </snapshots>
        <url>https://api.bitbucket.org/1.0/repositories/narrar3/mvnrelease/raw/releases</url>
    </repository>
    <repository>
        <id>narrarSnapshot</id>
        <name>narrarSnapshotName</name>
        <releases>
            <enabled>true</enabled>
        </releases>
        <snapshots>
            <enabled>true</enabled>
        </snapshots>
        <url>https://api.bitbucket.org/1.0/repositories/narrar3/mvnsnapshot/raw/snapshots</url>
    </repository>
</repositories>
```



### Who do I talk to? ###

jose